export default {
  logger: {
    level: 'debug',
    transport: ['file', 'console'],
    exceptionFile: 'logs/exception.log',
    logFile: 'logs/console.log',
  },
  middlewares: {
    pre: [
      { ids: 'hrefstart' },
      { __ssdGlobalMiddlewares__: 'sd_7EmxxOk703exD5hF' },
      { __ssdGlobalMiddlewares__: 'cors' },
    ],
    post: [],
    sequences: { IDSAuthroizedAPIs: { pre: [{ ids: 'Authorize' }], post: [] } },
  },
  ids: {
    client_id: '4kTyXAdVxb1FZVL5RAGWM',
    client_secret:
      'ZBJBHPzORoPFdXeJ2bEVeY7biP8EyeADJTPW8KyAVDzR0xLCjVV8oeafFq7mLvoO8AnHp2MaY1RV6ah3wikH1A',
    issuerURL: 'https://ids.neutrinos.co',
    enabled: true,
  },
};
