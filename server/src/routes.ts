//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-idsutil-sd_4iEzJnvaHM45KcZP
import { idsutil as sd_4iEzJnvaHM45KcZP } from './sd-services/idsutil';
//CORE_REFERENCE_IMPORT-ids-sd_tvIHK6hvO22JSnMT
import { ids as sd_tvIHK6hvO22JSnMT } from './sd-services/ids';
//CORE_REFERENCE_IMPORT-incident-SSD_SERVICE_ID_sd_X0Xzz0yuee2HGhiq
import { incident as SSD_SERVICE_ID_sd_X0Xzz0yuee2HGhiq } from './sd-services/incident';

export const UserRoutes = [
    //CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY
//CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY-idsutil-sd_4iEzJnvaHM45KcZP
sd_4iEzJnvaHM45KcZP,
//CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY-ids-sd_tvIHK6hvO22JSnMT
sd_tvIHK6hvO22JSnMT,
//CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY-incident-SSD_SERVICE_ID_sd_X0Xzz0yuee2HGhiq
SSD_SERVICE_ID_sd_X0Xzz0yuee2HGhiq,
];