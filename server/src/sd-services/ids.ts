let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { StatusCodes as httpStatusCodes } from 'http-status-codes'; //_splitter_
import * as cookieParser from 'cookie-parser'; //_splitter_
import { Readable } from 'stream'; //_splitter_
import { setInterval } from 'timers'; //_splitter_
import { Issuer, custom } from 'openid-client'; //_splitter_
import * as crypto from 'crypto'; //_splitter_
import * as url from 'url'; //_splitter_
import { SDBaseService } from '../services/SDBaseService'; //_splitter_
import { Middleware } from '../middleware/Middleware'; //_splitter_
import * as settings from '../config/config'; //_splitter_
import log from '../utils/Logger'; //_splitter_
import * as sd_4iEzJnvaHM45KcZP from './idsutil'; //_splitter_
//append_imports_end
export class ids {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'ids';
    this.app = app;
    this.serviceBasePath = this.app.settings.base;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new ids(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
      this.mountAllListeners();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountAllListeners() {
    try {
      //append_listeners
    } catch (e) {
      throw e;
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_ids_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: ids');

    let mw_hrefstart: Middleware = new Middleware(
      this.serviceName,
      'hrefstart',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_RpeKUVDeuJSWXu0m(bh);
          //appendnew_next_sd_Ia4is4HaW5FI8Ny1
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_Ia4is4HaW5FI8Ny1');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['hrefstart'] = mw_hrefstart;
    let mw_Authorize: Middleware = new Middleware(
      this.serviceName,
      'Authorize',
      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault({ local: {} }, req, res, next);
          bh = await this.sd_hiKTlXoDOXCjz9Ly(bh);
          //appendnew_next_sd_FF1Md3LYWd219XQq
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_FF1Md3LYWd219XQq');
        }
      }
    );
    this.generatedMiddlewares[this.serviceName]['Authorize'] = mw_Authorize;
    //appendnew_flow_ids_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: ids');

    if (!this.swaggerDocument['paths']['/login']) {
      this.swaggerDocument['paths']['/login'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/login']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/login`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_upRH0ngYajJrulcM(bh);
          //appendnew_next_sd_0JyCNDu5RRwhCATW
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_0JyCNDu5RRwhCATW');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/login/cb']) {
      this.swaggerDocument['paths']['/login/cb'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/login/cb']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/login/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_xLaJExSZrpfABBuC(bh);
          //appendnew_next_sd_eIGBZ4YErNC2qB6U
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_eIGBZ4YErNC2qB6U');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/user/info']) {
      this.swaggerDocument['paths']['/user/info'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/user/info']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/user/info`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_S9VDpZpsePxuGCYr(bh);
          //appendnew_next_sd_s5kfDB65OWT5Gsf2
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_s5kfDB65OWT5Gsf2');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        'IDSAuthroizedAPIs',
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/logout']) {
      this.swaggerDocument['paths']['/logout'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/logout']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/logout`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_EI2E6l6S55MRWLDO(bh);
          //appendnew_next_sd_Wl1MYWjl5Ie5b9iD
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_Wl1MYWjl5Ie5b9iD');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/logout/cb']) {
      this.swaggerDocument['paths']['/logout/cb'] = {
        get: {
          summary: '',
          description: '',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/logout/cb']['get'] = {
        summary: '',
        description: '',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/logout/cb`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_SVPf3U4BrfG6F0pE(bh);
          //appendnew_next_sd_IA47tlJGFBCRgX9C
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_IA47tlJGFBCRgX9C');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_ids_HttpIn
  }
  //   service flows_ids

  //appendnew_flow_ids_start

  async sd_upRH0ngYajJrulcM(bh) {
    try {
      bh.local.idsConfigured = false;
      if (
        settings.default.hasOwnProperty('ids') &&
        settings.default['ids'].hasOwnProperty('client_id') &&
        settings.default['ids'].hasOwnProperty('client_secret')
      ) {
        bh.local.idsConfigured = true;
      }
      bh = await this.sd_UYiqRYh1sOy7H6NZ(bh);
      //appendnew_next_sd_upRH0ngYajJrulcM
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_upRH0ngYajJrulcM');
    }
  }

  async sd_UYiqRYh1sOy7H6NZ(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.idsConfigured,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_QCRRwX2KrcFa1Q4o(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_Wat2I6SS3N3itoWe(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_UYiqRYh1sOy7H6NZ');
    }
  }

  async sd_QCRRwX2KrcFa1Q4o(bh) {
    try {
      bh.local.reqParams = {
        state: crypto.randomBytes(16).toString('hex'),
        nonce: crypto.randomBytes(16).toString('hex'),
        isMobile: bh.input.query.isMobile,
        redirectTo: bh.input.query.redirectTo,
      };
      bh = await this.sd_EDTQlpzzgl8T59WD(bh);
      //appendnew_next_sd_QCRRwX2KrcFa1Q4o
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_QCRRwX2KrcFa1Q4o');
    }
  }

  async sd_EDTQlpzzgl8T59WD(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.reqParams;
      }
      bh = await this.sd_CwQyTOqEfsy8cWI7(bh);
      //appendnew_next_sd_EDTQlpzzgl8T59WD
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_EDTQlpzzgl8T59WD');
    }
  }

  async sd_CwQyTOqEfsy8cWI7(bh) {
    try {
      const sd_4iEzJnvaHM45KcZPInstance: sd_4iEzJnvaHM45KcZP.idsutil = sd_4iEzJnvaHM45KcZP.idsutil.getInstance();
      let outputVariables = await sd_4iEzJnvaHM45KcZPInstance.getIDSClientInstance(
        null
      );
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_dibF0Rlv9vKfjtnb(bh);
      //appendnew_next_sd_CwQyTOqEfsy8cWI7
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_CwQyTOqEfsy8cWI7');
    }
  }

  async sd_dibF0Rlv9vKfjtnb(bh) {
    try {
      const sd_4iEzJnvaHM45KcZPInstance: sd_4iEzJnvaHM45KcZP.idsutil = sd_4iEzJnvaHM45KcZP.idsutil.getInstance();
      let outputVariables = await sd_4iEzJnvaHM45KcZPInstance.getAuthorizationParams(
        null
      );
      bh.input.authParams = outputVariables.input.authParams;

      bh = await this.sd_td8Pu8eOoSUlypUn(bh);
      //appendnew_next_sd_dibF0Rlv9vKfjtnb
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_dibF0Rlv9vKfjtnb');
    }
  }

  async sd_td8Pu8eOoSUlypUn(bh) {
    try {
      const authorizationRequest = Object.assign(
        {
          redirect_uri: url.resolve(bh.web.req.href, '/api/login/cb'),
          scope: 'openid profile email address phone user',
          state: bh.local.reqParams.state,
          nonce: bh.local.reqParams.nonce,
          response_type: bh.input.client.response_types[0],
        },
        bh.input.authParams
      );

      bh.local.redirectHeaders = {
        location: bh.input.client.authorizationUrl(authorizationRequest),
      };

      await this.sd_8ebMulCQBileoKsh(bh);
      //appendnew_next_sd_td8Pu8eOoSUlypUn
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_td8Pu8eOoSUlypUn');
    }
  }

  async sd_8ebMulCQBileoKsh(bh) {
    try {
      bh.web.res.set(bh.local.redirectHeaders);

      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8ebMulCQBileoKsh');
    }
  }

  async sd_Wat2I6SS3N3itoWe(bh) {
    try {
      bh.local.res = {
        message:
          'IDS client not registered. Register on the Neutrinos Stuido and try again',
      };
      await this.sd_UfW4jOAY9SWAo02D(bh);
      //appendnew_next_sd_Wat2I6SS3N3itoWe
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Wat2I6SS3N3itoWe');
    }
  }

  async sd_UfW4jOAY9SWAo02D(bh) {
    try {
      bh.web.res.status(404).send(bh.local.res.message);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_UfW4jOAY9SWAo02D');
    }
  }

  async sd_RpeKUVDeuJSWXu0m(bh) {
    try {
      const protocol =
        bh.input.headers['x-forwarded-proto'] || bh.web.req.protocol;
      const href =
        protocol + '://' + bh.web.req.get('Host') + bh.web.req.originalUrl;
      bh.web.req.href = href;
      await this.sd_0YxBGUOpoI2YVjJK(bh);
      //appendnew_next_sd_RpeKUVDeuJSWXu0m
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_RpeKUVDeuJSWXu0m');
    }
  }

  async sd_0YxBGUOpoI2YVjJK(bh) {
    try {
      bh.web.next();
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0YxBGUOpoI2YVjJK');
    }
  }

  async sd_xLaJExSZrpfABBuC(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.input.sessionParams = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_XXIejnRqUQM6RR00(bh);
      //appendnew_next_sd_xLaJExSZrpfABBuC
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_xLaJExSZrpfABBuC');
    }
  }

  async sd_XXIejnRqUQM6RR00(bh) {
    try {
      const sd_4iEzJnvaHM45KcZPInstance: sd_4iEzJnvaHM45KcZP.idsutil = sd_4iEzJnvaHM45KcZP.idsutil.getInstance();
      let outputVariables = await sd_4iEzJnvaHM45KcZPInstance.getIDSClientInstance(
        null
      );
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_spoj0y3Z4p7QGQSU(bh);
      //appendnew_next_sd_XXIejnRqUQM6RR00
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XXIejnRqUQM6RR00');
    }
  }

  async sd_spoj0y3Z4p7QGQSU(bh) {
    try {
      const params = bh.input.client.callbackParams(bh.web.req);
      let tokenset = await bh.input.client.callback(
        url.resolve(bh.web.req.href, 'cb'),
        params,
        {
          nonce: bh.input.sessionParams.data.nonce,
          state: bh.input.sessionParams.data.state,
        }
      );

      bh.local.redirectTo = bh.input.sessionParams.data.redirectTo;

      bh.local.userDetails = {
        tokenset: Object.assign({}, tokenset),
        userInfo: await bh.input.client.userinfo(tokenset['access_token']),
      };
      bh.local.userDetails['tokenset']['claims'] = Object.assign(
        {},
        tokenset.claims()
      );
      bh = await this.sd_t6RpEL5cUDW7h4SI(bh);
      //appendnew_next_sd_spoj0y3Z4p7QGQSU
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_spoj0y3Z4p7QGQSU');
    }
  }

  async sd_t6RpEL5cUDW7h4SI(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.userDetails;
      }
      bh = await this.sd_gvrWgVztalDph2S2(bh);
      //appendnew_next_sd_t6RpEL5cUDW7h4SI
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_t6RpEL5cUDW7h4SI');
    }
  }

  async sd_gvrWgVztalDph2S2(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.input.sessionParams.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_vXUtm5R7tEpAe8FQ(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_x596bgHw8QD78Ikm(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_gvrWgVztalDph2S2');
    }
  }

  async sd_vXUtm5R7tEpAe8FQ(bh) {
    try {
      bh.local.htmlResponse = `
 <html>
   <script>
      let _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;
      await this.sd_u9R3cqGEV0jj875t(bh);
      //appendnew_next_sd_vXUtm5R7tEpAe8FQ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_vXUtm5R7tEpAe8FQ');
    }
  }

  async sd_u9R3cqGEV0jj875t(bh) {
    try {
      bh.web.res.status(200).send(bh.local.htmlResponse);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_u9R3cqGEV0jj875t');
    }
  }

  async sd_x596bgHw8QD78Ikm(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.redirectTo,
      };
      await this.sd_9SGIw0EbAO9gfmMb(bh);
      //appendnew_next_sd_x596bgHw8QD78Ikm
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_x596bgHw8QD78Ikm');
    }
  }

  async sd_9SGIw0EbAO9gfmMb(bh) {
    try {
      bh.web.res.set(bh.local.redirectHeaders);

      bh.web.res.status(302).send('Redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_9SGIw0EbAO9gfmMb');
    }
  }

  async sd_S9VDpZpsePxuGCYr(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.session = JSON.parse(JSON.stringify(requestObject.session));
      }

      await this.sd_DL5dFLclS5ebF89g(bh);
      //appendnew_next_sd_S9VDpZpsePxuGCYr
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_S9VDpZpsePxuGCYr');
    }
  }

  async sd_DL5dFLclS5ebF89g(bh) {
    try {
      bh.web.res.status(200).send(bh.local.session.data.userInfo);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_DL5dFLclS5ebF89g');
    }
  }

  async sd_PFFnOeZQfnwEThFa(bh) {
    try {
      bh.web.res.redirect('/api/login');
      //appendnew_next_sd_PFFnOeZQfnwEThFa
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_PFFnOeZQfnwEThFa');
    }
  }

  async sd_EI2E6l6S55MRWLDO(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.sessionData = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_vcvEQRzLwP0VPMsx(bh);
      //appendnew_next_sd_EI2E6l6S55MRWLDO
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_EI2E6l6S55MRWLDO');
    }
  }

  async sd_vcvEQRzLwP0VPMsx(bh) {
    try {
      bh.local.sessionExists = false;
      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset
      ) {
        bh.local.sessionData['data']['redirectTo'] =
          bh.input.query['redirectTo'];
        bh.local.sessionData['data']['isMobile'] = bh.input.query['isMobile'];
        bh.local.sessionExists = true;
      } else {
        delete bh.local.sessionData['redirectTo'];
      }
      bh = await this.sd_zdraB64RzIlhBSaj(bh);
      //appendnew_next_sd_vcvEQRzLwP0VPMsx
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_vcvEQRzLwP0VPMsx');
    }
  }

  async sd_zdraB64RzIlhBSaj(bh) {
    try {
      const sd_4iEzJnvaHM45KcZPInstance: sd_4iEzJnvaHM45KcZP.idsutil = sd_4iEzJnvaHM45KcZP.idsutil.getInstance();
      let outputVariables = await sd_4iEzJnvaHM45KcZPInstance.getIDSClientInstance(
        null
      );
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_JXfFzOHaa3wjJR3Y(bh);
      //appendnew_next_sd_zdraB64RzIlhBSaj
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zdraB64RzIlhBSaj');
    }
  }

  async sd_JXfFzOHaa3wjJR3Y(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_EXB5ELv4UBsemhmW(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_azadxLbHNCr7oF8d(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_JXfFzOHaa3wjJR3Y');
    }
  }

  async sd_EXB5ELv4UBsemhmW(bh) {
    try {
      await Promise.all([
        bh.local.sessionData.data.tokenset.access_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.access_token,
              'access_token'
            )
          : undefined,
        bh.local.sessionData.data.tokenset.refresh_token
          ? bh.input.client.revoke(
              bh.local.sessionData.data.tokenset.refresh_token,
              'refresh_token'
            )
          : undefined,
      ]);

      bh.local.res = {
        idsURL: url.format(
          Object.assign(
            url.parse(bh.input.client.issuer.end_session_endpoint),
            {
              search: null,
              query: {
                id_token_hint: bh.local.sessionData.data.tokenset.id_token,
                post_logout_redirect_uri: url.resolve(
                  bh.web.req.href,
                  '/api/logout/cb'
                ),
                client_id: settings.default['ids']['client_id'],
              },
            }
          )
        ),
        sessionExists: true,
      };

      // Remove user info and tokenset before login redirect
      bh.local.sessionData.data.tokenset = null;
      bh.local.sessionData.data.userInfo = null;
      bh = await this.sd_dCWCI7Lqpk6tk1KJ(bh);
      //appendnew_next_sd_EXB5ELv4UBsemhmW
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_EXB5ELv4UBsemhmW');
    }
  }

  async sd_dCWCI7Lqpk6tk1KJ(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.sessionData.data;
      }
      await this.sd_VBHFmtk9OYKghiOd(bh);
      //appendnew_next_sd_dCWCI7Lqpk6tk1KJ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_dCWCI7Lqpk6tk1KJ');
    }
  }

  async sd_VBHFmtk9OYKghiOd(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VBHFmtk9OYKghiOd');
    }
  }

  async sd_azadxLbHNCr7oF8d(bh) {
    try {
      bh.local.res = {
        sessionExists: false,
      };
      await this.sd_VBHFmtk9OYKghiOd(bh);
      //appendnew_next_sd_azadxLbHNCr7oF8d
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_azadxLbHNCr7oF8d');
    }
  }

  async sd_SVPf3U4BrfG6F0pE(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.sessionData = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_lAdfpLaRdwvEWSdF(bh);
      //appendnew_next_sd_SVPf3U4BrfG6F0pE
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_SVPf3U4BrfG6F0pE');
    }
  }

  async sd_lAdfpLaRdwvEWSdF(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        let p = function () {
          return new Promise((resolve, reject) => {
            requestObject.session.destroy(function (error) {
              if (error) {
                return reject(error);
              }
              return resolve();
            });
          });
        };
        await p();
      }
      bh = await this.sd_Ay0J68wllelwMtrX(bh);
      //appendnew_next_sd_lAdfpLaRdwvEWSdF
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_lAdfpLaRdwvEWSdF');
    }
  }

  async sd_Ay0J68wllelwMtrX(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['se'](
          bh.local.sessionData.data.isMobile,
          'true',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_MPHWzL6JFWNmIMVm(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_lkvoAVWf13CUDBoR(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Ay0J68wllelwMtrX');
    }
  }

  async sd_MPHWzL6JFWNmIMVm(bh) {
    try {
      bh.local.res = `<html>
   <script>
      var _timer;
      _timer = setInterval(() => {
                  if(window.webkit) {
                      window.webkit.messageHandlers.cordova_iab.postMessage(JSON.stringify({'auth': 'success'}));
                      clearInterval(_timer);
                  }
              }, 250);
      
   </script>
</html>`;
      await this.sd_6Nm9TQcmjH1TWuXe(bh);
      //appendnew_next_sd_MPHWzL6JFWNmIMVm
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_MPHWzL6JFWNmIMVm');
    }
  }

  async sd_6Nm9TQcmjH1TWuXe(bh) {
    try {
      bh.web.res.status(200).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_6Nm9TQcmjH1TWuXe');
    }
  }

  async sd_lkvoAVWf13CUDBoR(bh) {
    try {
      bh.local.redirectHeaders = {
        location: bh.local.sessionData.data.redirectTo,
      };
      await this.sd_UJBplWgljbZ10IET(bh);
      //appendnew_next_sd_lkvoAVWf13CUDBoR
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_lkvoAVWf13CUDBoR');
    }
  }

  async sd_UJBplWgljbZ10IET(bh) {
    try {
      bh.web.res.set(bh.local.redirectHeaders);

      bh.web.res.status(302).send('redirecting');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_UJBplWgljbZ10IET');
    }
  }

  async sd_hiKTlXoDOXCjz9Ly(bh) {
    try {
      bh.local = {};
      bh = await this.sd_VJFWuoVMb9p8YZCF(bh);
      //appendnew_next_sd_hiKTlXoDOXCjz9Ly
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_hiKTlXoDOXCjz9Ly');
    }
  }

  async sd_VJFWuoVMb9p8YZCF(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        bh.local.sessionData = JSON.parse(
          JSON.stringify(requestObject.session)
        );
      }

      bh = await this.sd_VahsiaQEwnvfLus9(bh);
      //appendnew_next_sd_VJFWuoVMb9p8YZCF
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VJFWuoVMb9p8YZCF');
    }
  }

  async sd_VahsiaQEwnvfLus9(bh) {
    try {
      bh.local.sessionExists = false;

      if (
        bh.local.sessionData &&
        bh.local.sessionData.data &&
        bh.local.sessionData.data.tokenset &&
        bh.local.sessionData.data.tokenset.access_token &&
        bh.local.sessionData.data.tokenset.refresh_token
      ) {
        bh.local.sessionExists = true;
      }
      bh = await this.sd_Vy0vWMt7rsMQBspO(bh);
      //appendnew_next_sd_VahsiaQEwnvfLus9
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VahsiaQEwnvfLus9');
    }
  }

  async sd_Vy0vWMt7rsMQBspO(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.sessionExists,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_RpsTc4BQr9CbaFGK(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_4q4zkQ83sAJws6BJ(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Vy0vWMt7rsMQBspO');
    }
  }

  async sd_RpsTc4BQr9CbaFGK(bh) {
    try {
      const sd_4iEzJnvaHM45KcZPInstance: sd_4iEzJnvaHM45KcZP.idsutil = sd_4iEzJnvaHM45KcZP.idsutil.getInstance();
      let outputVariables = await sd_4iEzJnvaHM45KcZPInstance.handleTokenExpiry(
        bh.local.sessionData,
        null
      );
      bh.local.newSession = outputVariables.input.newSession;

      bh = await this.sd_sIOUUVZioyWPvtcJ(bh);
      //appendnew_next_sd_RpsTc4BQr9CbaFGK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_RpsTc4BQr9CbaFGK');
    }
  }

  async sd_sIOUUVZioyWPvtcJ(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['false'](
          bh.local.newSession,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_7yKcJRskGfqbfHCQ(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_P1vKPomPjIMAgblD(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_sIOUUVZioyWPvtcJ');
    }
  }

  async sd_7yKcJRskGfqbfHCQ(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        let p = function () {
          return new Promise((resolve, reject) => {
            requestObject.session.destroy(function (error) {
              if (error) {
                return reject(error);
              }
              return resolve();
            });
          });
        };
        await p();
      }
      bh = await this.sd_bGSbtXBmNwmjyKzT(bh);
      //appendnew_next_sd_7yKcJRskGfqbfHCQ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_7yKcJRskGfqbfHCQ');
    }
  }

  async sd_bGSbtXBmNwmjyKzT(bh) {
    try {
      bh.local.res = {
        code: 'TOKEN_EXPIRED',
        message: 'Token invalid or access revoked',
      };
      await this.sd_NfxyxrpN5RrZl2kt(bh);
      //appendnew_next_sd_bGSbtXBmNwmjyKzT
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_bGSbtXBmNwmjyKzT');
    }
  }

  async sd_NfxyxrpN5RrZl2kt(bh) {
    try {
      bh.web.res.status(403).send(bh.local.res);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_NfxyxrpN5RrZl2kt');
    }
  }

  async sd_P1vKPomPjIMAgblD(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.newSession.rotated,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_m9K7Y9hyZXFMEURG(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_4nXVLoX5GigmWn1d(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_P1vKPomPjIMAgblD');
    }
  }

  async sd_m9K7Y9hyZXFMEURG(bh) {
    try {
      delete bh.local.newSession.rotated;
      bh = await this.sd_8jI4V8iEkQW5aB2C(bh);
      //appendnew_next_sd_m9K7Y9hyZXFMEURG
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_m9K7Y9hyZXFMEURG');
    }
  }

  async sd_8jI4V8iEkQW5aB2C(bh) {
    try {
      let requestObject = bh.web.req;
      if (requestObject.session) {
        requestObject.session.data = bh.local.newSession;
      }
      await this.sd_4nXVLoX5GigmWn1d(bh);
      //appendnew_next_sd_8jI4V8iEkQW5aB2C
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8jI4V8iEkQW5aB2C');
    }
  }

  async sd_4nXVLoX5GigmWn1d(bh) {
    try {
      bh.web.next();
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_4nXVLoX5GigmWn1d');
    }
  }

  async sd_4q4zkQ83sAJws6BJ(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['cont'](
          bh.input.path,
          '/user/info',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_aZWCVKtS1gt4dOLO(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_3krD2Y3STrSttrCt(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_4q4zkQ83sAJws6BJ');
    }
  }

  async sd_aZWCVKtS1gt4dOLO(bh) {
    try {
      bh.local.res = { message: 'Session expired' };
      await this.sd_NfxyxrpN5RrZl2kt(bh);
      //appendnew_next_sd_aZWCVKtS1gt4dOLO
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_aZWCVKtS1gt4dOLO');
    }
  }

  async sd_3krD2Y3STrSttrCt(bh) {
    try {
      bh.local.res = { code: 'NO_SESSION', message: 'Session not present' };
      await this.sd_NfxyxrpN5RrZl2kt(bh);
      //appendnew_next_sd_3krD2Y3STrSttrCt
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_3krD2Y3STrSttrCt');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_q7zjQPgdQncSLZoU(bh)) ||
      (await this.sd_6awafoLFFhuKy3Y5(bh))
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      if (bh.web.next) {
        bh.web.next(e);
      } else {
        throw e;
      }
    }
  }
  async sd_q7zjQPgdQncSLZoU(bh) {
    const nodes = [
      'sd_dibF0Rlv9vKfjtnb',
      'sd_eIGBZ4YErNC2qB6U',
      'sd_XXIejnRqUQM6RR00',
      'sd_spoj0y3Z4p7QGQSU',
      'sd_xLaJExSZrpfABBuC',
      'sd_gvrWgVztalDph2S2',
      'sd_vXUtm5R7tEpAe8FQ',
      'sd_x596bgHw8QD78Ikm',
      'sd_u9R3cqGEV0jj875t',
      'sd_9SGIw0EbAO9gfmMb',
    ];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_PFFnOeZQfnwEThFa(bh);
      //appendnew_next_sd_q7zjQPgdQncSLZoU
      return true;
    }
    return false;
  }
  async sd_6awafoLFFhuKy3Y5(bh) {
    const nodes = ['sd_RpsTc4BQr9CbaFGK'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_bGSbtXBmNwmjyKzT(bh);
      //appendnew_next_sd_6awafoLFFhuKy3Y5
      return true;
    }
    return false;
  }
  //appendnew_flow_ids_Catch
}
