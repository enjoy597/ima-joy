let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { StatusCodes as httpStatusCodes } from 'http-status-codes'; //_splitter_
import * as cookieParser from 'cookie-parser'; //_splitter_
import { Readable } from 'stream'; //_splitter_
import { setInterval } from 'timers'; //_splitter_
import { Issuer, custom } from 'openid-client'; //_splitter_
import * as crypto from 'crypto'; //_splitter_
import * as url from 'url'; //_splitter_
import { SDBaseService } from '../services/SDBaseService'; //_splitter_
import { Middleware } from '../middleware/Middleware'; //_splitter_
import * as settings from '../config/config'; //_splitter_
import log from '../utils/Logger'; //_splitter_
import { GenericRDBMSOperations } from '../utils/ndefault-sql/ExecuteSql/GenericRDBMSOperations'; //_splitter_
//append_imports_end
export class incident {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'incident';
    this.app = app;
    this.serviceBasePath = this.app.settings.base;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new incident(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
      this.mountAllListeners();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountAllListeners() {
    try {
      //append_listeners
    } catch (e) {
      throw e;
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_incident_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: incident');

    //appendnew_flow_incident_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: incident');

    if (!this.swaggerDocument['paths']['/addincident']) {
      this.swaggerDocument['paths']['/addincident'] = {
        post: {
          summary: 'Add incident',
          description: 'API to add a new incident',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/addincident']['post'] = {
        summary: 'Add incident',
        description: 'API to add a new incident',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['post'](
      `${this.serviceBasePath}/addincident`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_kgQFn9rMJ0TjQxFS(bh);
          //appendnew_next_sd_A801uODWIWYfRYX2
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_A801uODWIWYfRYX2');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/getallincident']) {
      this.swaggerDocument['paths']['/getallincident'] = {
        get: {
          summary: 'Incident list',
          description: 'API to get all the incident',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/getallincident']['get'] = {
        summary: 'Incident list',
        description: 'API to get all the incident',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['get'](
      `${this.serviceBasePath}/getallincident`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_yfg6dX944lHzePup(bh);
          //appendnew_next_sd_fI36Yrk7BQ4HSsuL
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_fI36Yrk7BQ4HSsuL');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/updateincident']) {
      this.swaggerDocument['paths']['/updateincident'] = {
        put: {
          summary: 'update Incident',
          description: 'API to update the incident',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/updateincident']['put'] = {
        summary: 'update Incident',
        description: 'API to update the incident',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['put'](
      `${this.serviceBasePath}/updateincident`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_z8SJpatL8ogCfloK(bh);
          //appendnew_next_sd_AxXzxaFpSdR51YAG
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_AxXzxaFpSdR51YAG');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );

    if (!this.swaggerDocument['paths']['/deleteincident']) {
      this.swaggerDocument['paths']['/deleteincident'] = {
        delete: {
          summary: 'Incident list',
          description: 'API to delete the incident',
          consumes: [],
          produces: [],
          parameters: [],
          responses: {},
        },
      };
    } else {
      this.swaggerDocument['paths']['/deleteincident']['delete'] = {
        summary: 'Incident list',
        description: 'API to delete the incident',
        consumes: [],
        produces: [],
        parameters: [],
        responses: {},
      };
    }
    this.app['delete'](
      `${this.serviceBasePath}/deleteincident`,
      cookieParser(),
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'pre',
        this.generatedMiddlewares
      ),

      async (req, res, next) => {
        let bh = {};
        try {
          bh = this.sdService.__constructDefault(
            { local: {}, input: {} },
            req,
            res,
            next
          );
          bh = await this.sd_nBRbRcGCfbEf6rfS(bh);
          //appendnew_next_sd_jIrWxkEycz0ahaiZ
        } catch (e) {
          return await this.errorHandler(bh, e, 'sd_jIrWxkEycz0ahaiZ');
        }
      },
      this.sdService.getMiddlesWaresBySequenceId(
        null,
        'post',
        this.generatedMiddlewares
      )
    );
    //appendnew_flow_incident_HttpIn
  }
  //   service flows_incident

  //appendnew_flow_incident_start

  async sd_kgQFn9rMJ0TjQxFS(bh) {
    try {
      const moment = require('moment');

      console.log(bh.input.body);
      var newDate = moment(bh.input.body.date).format('YYYY-MM-DD hh:mm:ss');
      bh.local.query = `INSERT INTO incident(subject,description,priority,date_of_incident)
 VALUES('${bh.input.body.subject}','${bh.input.body.description}',
 '${bh.input.body.priority}','${newDate}');`;

      console.log(bh.local.query);
      bh = await this.sd_3oIlg1vFkREpNzzl(bh);
      //appendnew_next_sd_kgQFn9rMJ0TjQxFS
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_kgQFn9rMJ0TjQxFS');
    }
  }

  async sd_3oIlg1vFkREpNzzl(bh) {
    try {
      let configObj = this.sdService.getConfigObj(
        'db-config',
        'sd_DhfRo8Gq1JcBm2LJ'
      );
      let connectionName;
      if (
        configObj &&
        configObj.hasOwnProperty('dbOption') &&
        configObj.dbOption.hasOwnProperty('name')
      ) {
        connectionName = configObj.dbOption.name;
      } else {
        throw new Error('Cannot find the selected config name');
      }
      let params = undefined;
      params = params ? params : [];
      bh.local.result = await new GenericRDBMSOperations().executeSQL(
        connectionName,
        bh.local.query,
        params
      );
      await this.sd_hE8l79eZ0F0emqw8(bh);
      //appendnew_next_sd_3oIlg1vFkREpNzzl
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_3oIlg1vFkREpNzzl');
    }
  }

  async sd_hE8l79eZ0F0emqw8(bh) {
    try {
      bh.web.res.status(200).send('new incident is added into database');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_hE8l79eZ0F0emqw8');
    }
  }

  async sd_yfg6dX944lHzePup(bh) {
    try {
      bh.local.query = `SELECT * FROM incident;`;
      bh = await this.sd_QCV8efdJYMG3wcdt(bh);
      //appendnew_next_sd_yfg6dX944lHzePup
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_yfg6dX944lHzePup');
    }
  }

  async sd_QCV8efdJYMG3wcdt(bh) {
    try {
      let configObj = this.sdService.getConfigObj(
        'db-config',
        'sd_DhfRo8Gq1JcBm2LJ'
      );
      let connectionName;
      if (
        configObj &&
        configObj.hasOwnProperty('dbOption') &&
        configObj.dbOption.hasOwnProperty('name')
      ) {
        connectionName = configObj.dbOption.name;
      } else {
        throw new Error('Cannot find the selected config name');
      }
      let params = undefined;
      params = params ? params : [];
      bh.local.result = await new GenericRDBMSOperations().executeSQL(
        connectionName,
        bh.local.query,
        params
      );
      await this.sd_7a6VYbWa3zMdDVIB(bh);
      //appendnew_next_sd_QCV8efdJYMG3wcdt
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_QCV8efdJYMG3wcdt');
    }
  }

  async sd_7a6VYbWa3zMdDVIB(bh) {
    try {
      bh.web.res.status(200).send(bh.local.result);

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_7a6VYbWa3zMdDVIB');
    }
  }

  async sd_z8SJpatL8ogCfloK(bh) {
    try {
      const moment = require('moment');

      console.log(bh.input.body);
      var newDate = moment(bh.input.body.date).format('YYYY-MM-DD hh:mm:ss');

      bh.local.query = `UPDATE incident SET subject='${bh.input.body.subject}',
description='${bh.input.body.description}',
priority='${bh.input.body.priority}',
date_of_incident='${newDate}'
 where id='${bh.input.body.id}';`;
      bh = await this.sd_fzgV66jpVMtx82PH(bh);
      //appendnew_next_sd_z8SJpatL8ogCfloK
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_z8SJpatL8ogCfloK');
    }
  }

  async sd_fzgV66jpVMtx82PH(bh) {
    try {
      let configObj = this.sdService.getConfigObj(
        'db-config',
        'sd_DhfRo8Gq1JcBm2LJ'
      );
      let connectionName;
      if (
        configObj &&
        configObj.hasOwnProperty('dbOption') &&
        configObj.dbOption.hasOwnProperty('name')
      ) {
        connectionName = configObj.dbOption.name;
      } else {
        throw new Error('Cannot find the selected config name');
      }
      let params = undefined;
      params = params ? params : [];
      bh.local.result = await new GenericRDBMSOperations().executeSQL(
        connectionName,
        bh.local.query,
        params
      );
      await this.sd_lSYpmRRpDfuSs9g7(bh);
      //appendnew_next_sd_fzgV66jpVMtx82PH
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_fzgV66jpVMtx82PH');
    }
  }

  async sd_lSYpmRRpDfuSs9g7(bh) {
    try {
      bh.web.res.status(200).send('incident is updated');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_lSYpmRRpDfuSs9g7');
    }
  }

  async sd_nBRbRcGCfbEf6rfS(bh) {
    try {
      console.log(bh.input);
      bh.local.query = `DELETE FROM incident where id=${bh.input.query.id};`;
      bh = await this.sd_BmUBfCSsJLW3qc35(bh);
      //appendnew_next_sd_nBRbRcGCfbEf6rfS
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_nBRbRcGCfbEf6rfS');
    }
  }

  async sd_BmUBfCSsJLW3qc35(bh) {
    try {
      let configObj = this.sdService.getConfigObj(
        'db-config',
        'sd_DhfRo8Gq1JcBm2LJ'
      );
      let connectionName;
      if (
        configObj &&
        configObj.hasOwnProperty('dbOption') &&
        configObj.dbOption.hasOwnProperty('name')
      ) {
        connectionName = configObj.dbOption.name;
      } else {
        throw new Error('Cannot find the selected config name');
      }
      let params = undefined;
      params = params ? params : [];
      bh.local.result = await new GenericRDBMSOperations().executeSQL(
        connectionName,
        bh.local.query,
        params
      );
      await this.sd_Oe2dk8cjjeZolog3(bh);
      //appendnew_next_sd_BmUBfCSsJLW3qc35
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_BmUBfCSsJLW3qc35');
    }
  }

  async sd_Oe2dk8cjjeZolog3(bh) {
    try {
      bh.web.res.status(200).send('incident remove');

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Oe2dk8cjjeZolog3');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      if (bh.web.next) {
        bh.web.next(e);
      } else {
        throw e;
      }
    }
  }
  //appendnew_flow_incident_Catch
}
