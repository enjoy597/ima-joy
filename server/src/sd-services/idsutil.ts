let instance = null;
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { StatusCodes as httpStatusCodes } from 'http-status-codes'; //_splitter_
import * as cookieParser from 'cookie-parser'; //_splitter_
import { Readable } from 'stream'; //_splitter_
import { setInterval } from 'timers'; //_splitter_
import { Issuer, custom } from 'openid-client'; //_splitter_
import * as crypto from 'crypto'; //_splitter_
import * as url from 'url'; //_splitter_
import { SDBaseService } from '../services/SDBaseService'; //_splitter_
import { Middleware } from '../middleware/Middleware'; //_splitter_
import * as settings from '../config/config'; //_splitter_
import log from '../utils/Logger'; //_splitter_
//append_imports_end
export class idsutil {
  private sdService = new SDBaseService();
  private app;
  private serviceBasePath: string;
  private generatedMiddlewares: Object;
  private serviceName: string;
  private swaggerDocument: Object;
  private globalTimers: any;
  private constructor(
    app,
    generatedeMiddlewares,
    routeCall,
    middlewareCall,
    swaggerDocument,
    globalTimers
  ) {
    this.serviceName = 'idsutil';
    this.app = app;
    this.serviceBasePath = this.app.settings.base;
    this.generatedMiddlewares = generatedeMiddlewares;
    this.swaggerDocument = swaggerDocument;
    this.globalTimers = globalTimers;
  }

  static getInstance(
    app?,
    generatedeMiddlewares?,
    routeCall?,
    middlewareCall?,
    swaggerDocument?,
    globalTimers?
  ) {
    if (!instance) {
      instance = new idsutil(
        app,
        generatedeMiddlewares,
        routeCall,
        middlewareCall,
        swaggerDocument,
        globalTimers
      );
    }
    instance.mountCalls(routeCall, middlewareCall);
    return instance;
  }

  private mountCalls(routeCall, middlewareCall) {
    if (routeCall) {
      this.mountAllPaths();
      this.mountAllListeners();
    }
    if (middlewareCall) {
      this.generatedMiddlewares[this.serviceName] = {};
      this.mountAllMiddlewares();
      this.mountTimers();
    }
  }

  async mountAllListeners() {
    try {
      //append_listeners
    } catch (e) {
      throw e;
    }
  }

  async mountTimers() {
    try {
      //appendnew_flow_idsutil_TimerStart
    } catch (e) {
      throw e;
    }
  }

  private mountAllMiddlewares() {
    log.debug('mounting all middlewares for service :: idsutil');

    //appendnew_flow_idsutil_MiddlewareStart
  }
  private mountAllPaths() {
    log.debug('mounting all paths for service :: idsutil');
    //appendnew_flow_idsutil_HttpIn
  }
  //   service flows_idsutil

  async getIDSClientInstance(clientInstance = null, ...others) {
    try {
      var bh = {
        input: {
          clientInstance: clientInstance,
        },
        local: {},
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_cLjYlP7Kcy7ybJFJ(bh);
      //appendnew_next_getIDSClientInstance
      return (
        // formatting output variables
        {
          input: {
            clientInstance: bh.input.clientInstance,
          },
          local: {},
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XUbbsnsxwKKSxvuX');
    }
  }

  async getAuthorizationParams(authParams = null, ...others) {
    try {
      var bh = {
        input: {
          authParams: authParams,
        },
        local: {},
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_VBB4qR0HC4N3fWjn(bh);
      //appendnew_next_getAuthorizationParams
      return (
        // formatting output variables
        {
          input: {
            authParams: bh.input.authParams,
          },
          local: {},
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8Ci450wqzr3HNzHo');
    }
  }

  async handleTokenExpiry(existingSession = '', newSession = '', ...others) {
    try {
      var bh = {
        input: {
          existingSession: existingSession,
          newSession: newSession,
        },
        local: {},
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_5s5Q3oR9r8hRJYLA(bh);
      //appendnew_next_handleTokenExpiry
      return (
        // formatting output variables
        {
          input: {
            newSession: bh.input.newSession,
          },
          local: {},
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_afhZ9qpCPIHdZvZq');
    }
  }

  //appendnew_flow_idsutil_start

  //__server_service_designer_class_variable_declaration__client
  client: any;
  async sd_cLjYlP7Kcy7ybJFJ(bh) {
    try {
      bh.local.client = this.client;
      bh = await this.sd_TlkYr2tA75yqIZCX(bh);
      //appendnew_next_sd_cLjYlP7Kcy7ybJFJ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_cLjYlP7Kcy7ybJFJ');
    }
  }

  async sd_TlkYr2tA75yqIZCX(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['istype'](
          bh.local.client,
          'undefined',
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_2DigVy9Z8aqo5Dgw(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_P1iM9GXnTzFkkwUm(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_TlkYr2tA75yqIZCX');
    }
  }

  async sd_2DigVy9Z8aqo5Dgw(bh) {
    try {
      const DEFAULT_HTTP_OPTIONS = {
        timeout: 60000,
      };

      custom.setHttpOptionsDefaults({
        timeout: DEFAULT_HTTP_OPTIONS.timeout,
      });
      log.info(
        `Identity server default HTTP options : ${DEFAULT_HTTP_OPTIONS}`
      );
      const issuer = await Issuer.discover(
        settings.default['ids']['issuerURL']
      );
      log.info(`Identity server discovered at : ${issuer.issuer}`);
      const client = await new issuer.Client({
        client_id: settings.default['ids']['client_id'],
        client_secret: settings.default['ids']['client_secret'],
      });
      client[custom.clock_tolerance] = process.env.CLOCK_TOLERANCE;
      log.info('Client connected...');
      bh.input.clientInstance = client;
      bh = await this.sd_7VEaEiQCjF124NzW(bh);
      //appendnew_next_sd_2DigVy9Z8aqo5Dgw
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_2DigVy9Z8aqo5Dgw');
    }
  }

  async sd_7VEaEiQCjF124NzW(bh) {
    try {
      this.client = bh.input.clientInstance;
      //appendnew_next_sd_7VEaEiQCjF124NzW
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_7VEaEiQCjF124NzW');
    }
  }

  async sd_P1iM9GXnTzFkkwUm(bh) {
    try {
      bh.input.clientInstance = this.client;
      //appendnew_next_sd_P1iM9GXnTzFkkwUm
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_P1iM9GXnTzFkkwUm');
    }
  }

  async sd_VBB4qR0HC4N3fWjn(bh) {
    try {
      bh.input.authParams = {
        scope: 'openid profile email address phone offline_access user',
        prompt: 'consent',
      };
      //appendnew_next_sd_VBB4qR0HC4N3fWjn
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_VBB4qR0HC4N3fWjn');
    }
  }

  async sd_5s5Q3oR9r8hRJYLA(bh) {
    try {
      const tokenset = bh.input.existingSession.data.tokenset;
      bh.local.expires_at = tokenset['expires_at'] * 1000;
      bh.local.refreshTime = new Date().valueOf() + 300000; // 5min before
      bh = await this.sd_Se6OPUeSqzwXzzWw(bh);
      //appendnew_next_sd_5s5Q3oR9r8hRJYLA
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_5s5Q3oR9r8hRJYLA');
    }
  }

  async sd_Se6OPUeSqzwXzzWw(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['gt'](
          bh.local.expires_at,
          bh.local.refreshTime,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_OpiI8lElfw0gVsKZ(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_XIXuGY5FnF9cpdXH(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Se6OPUeSqzwXzzWw');
    }
  }

  async sd_OpiI8lElfw0gVsKZ(bh) {
    try {
      bh.input.newSession = bh.input.existingSession.data;
      //appendnew_next_sd_OpiI8lElfw0gVsKZ
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_OpiI8lElfw0gVsKZ');
    }
  }

  async sd_XIXuGY5FnF9cpdXH(bh) {
    try {
      let outputVariables = await this.getIDSClientInstance(null);
      bh.input.client = outputVariables.input.clientInstance;

      bh = await this.sd_ZjDaMKTMC7k3aK3r(bh);
      //appendnew_next_sd_XIXuGY5FnF9cpdXH
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_XIXuGY5FnF9cpdXH');
    }
  }

  async sd_ZjDaMKTMC7k3aK3r(bh) {
    try {
      bh.local.refresh_token = await bh.input.client.introspect(
        bh.input.existingSession.data.tokenset.refresh_token,
        'refresh_token'
      );
      bh = await this.sd_ZwDN8qPYjKoZ7DFc(bh);
      //appendnew_next_sd_ZjDaMKTMC7k3aK3r
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ZjDaMKTMC7k3aK3r');
    }
  }

  async sd_ZwDN8qPYjKoZ7DFc(bh) {
    try {
      let otherwiseFlag = true;
      if (
        this.sdService.operators['true'](
          bh.local.refresh_token.active,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_8P2Ic8OwANTsNNLT(bh);
        otherwiseFlag = false;
      }
      if (
        this.sdService.operators['else'](
          otherwiseFlag,
          undefined,
          undefined,
          undefined
        )
      ) {
        bh = await this.sd_0ihkfB1HHBP5uTii(bh);
        otherwiseFlag = false;
      }

      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_ZwDN8qPYjKoZ7DFc');
    }
  }

  async sd_8P2Ic8OwANTsNNLT(bh) {
    try {
      bh.input.newSession = { rotated: true };
      bh.input.newSession['tokenset'] = await bh.input.client.refresh(
        bh.input.existingSession.data.tokenset.refresh_token
      );
      bh.input.newSession['userInfo'] = await bh.input.client.userinfo(
        bh.input.newSession['tokenset']['access_token']
      );
      bh.input.newSession['tokenset']['claims'] = Object.assign(
        {},
        bh.input.newSession['tokenset'].claims()
      );
      //appendnew_next_sd_8P2Ic8OwANTsNNLT
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8P2Ic8OwANTsNNLT');
    }
  }

  async sd_0ihkfB1HHBP5uTii(bh) {
    try {
      bh.input.newSession = false;
      //appendnew_next_sd_0ihkfB1HHBP5uTii
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0ihkfB1HHBP5uTii');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false ||
      (await this.sd_jO95FT7YxLeNsjkQ(bh))
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      if (bh.web.next) {
        bh.web.next(e);
      } else {
        throw e;
      }
    }
  }
  async sd_jO95FT7YxLeNsjkQ(bh) {
    const nodes = ['handleTokenExpiry'];
    if (nodes.includes(bh.errorSource)) {
      bh = await this.sd_0ihkfB1HHBP5uTii(bh);
      //appendnew_next_sd_jO95FT7YxLeNsjkQ
      return true;
    }
    return false;
  }
  //appendnew_flow_idsutil_Catch
}
