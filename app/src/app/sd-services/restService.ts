/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
//CORE_REFERENCE_IMPORTS
//append_imports_start

import { Injectable } from '@angular/core'; //_splitter_
import {
  Router,
  NavigationEnd,
  NavigationStart,
  Resolve,
  ActivatedRouteSnapshot,
} from '@angular/router'; //_splitter_
import { MatSnackBar } from '@angular/material/snack-bar'; //_splitter_
import { SDBaseService } from '../../app/n-services/SDBaseService'; //_splitter_
//append_imports_end

declare const window: any;
declare const cordova: any;

@Injectable()
export class restService {
  constructor(
    private sdService: SDBaseService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) {
    this.registerListeners();
  }
  registerListeners() {
    let bh = this.sdService.__constructDefault({});

    //append_listeners
  }

  //   service flows_restService

  async addIncident(incident: any = undefined, ...others) {
    try {
      var bh = {
        input: {
          incident: incident,
        },
        local: {},
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_tmq3a6l3nSLg77Z3(bh);
      //appendnew_next_addIncident
      return (
        // formatting output variables
        {
          input: {},
          local: {},
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_DG3prm4cujvGYzQS');
    }
  }

  async getAllIncident(...others) {
    try {
      var bh = {
        input: {},
        local: {
          result: undefined,
        },
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_0i1l8s1OsOAlqCzs(bh);
      //appendnew_next_getAllIncident
      return (
        // formatting output variables
        {
          input: {},
          local: {
            result: bh.local.result,
          },
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Wv4cff6tyyzUunsu');
    }
  }

  async updateIncident(incident: any = undefined, ...others) {
    try {
      var bh = {
        input: {
          incident: incident,
        },
        local: {
          result: undefined,
        },
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_sVpgJ4PjgZeH4opj(bh);
      //appendnew_next_updateIncident
      return (
        // formatting output variables
        {
          input: {},
          local: {
            result: bh.local.result,
          },
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_N0BvR8zYDhFyZyOp');
    }
  }

  async deleteIncident(id = 0, ...others) {
    try {
      var bh = {
        input: {
          id: id,
        },
        local: {
          result: undefined,
        },
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_8XZdP0HZxooR0xPL(bh);
      //appendnew_next_deleteIncident
      return (
        // formatting output variables
        {
          input: {},
          local: {
            result: bh.local.result,
          },
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_PWmxJLiQoC6N0kiH');
    }
  }

  async setLogin(isLogin: any = undefined, ...others) {
    try {
      var bh = {
        input: {
          isLogin: isLogin,
        },
        local: {},
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_Wil4WWsnczU8Pokg(bh);
      //appendnew_next_setLogin
      return (
        // formatting output variables
        {
          input: {},
          local: {},
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_kPIxBtYKYYPpPP5W');
    }
  }

  async isLoggedIn(...others) {
    try {
      var bh = {
        input: {},
        local: {
          isLogin: undefined,
        },
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_oNDJgAFs21TP0wSA(bh);
      //appendnew_next_isLoggedIn
      return (
        // formatting output variables
        {
          input: {},
          local: {
            isLogin: bh.local.isLogin,
          },
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_IDxDAjjc38ih1RUz');
    }
  }

  async setData(userData: any = undefined, ...others) {
    try {
      var bh = {
        input: {
          userData: userData,
        },
        local: {},
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_BzdqWFx0Rv9j8eH6(bh);
      //appendnew_next_setData
      return (
        // formatting output variables
        {
          input: {},
          local: {},
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Fsq8Lo5QedImiQ7n');
    }
  }

  async getData(...others) {
    try {
      var bh = {
        input: {},
        local: {
          userData: undefined,
        },
      };
      bh = this.sdService.__constructDefault(bh);
      bh = await this.sd_K3eU6nABk8arZwlC(bh);
      //appendnew_next_getData
      return (
        // formatting output variables
        {
          input: {},
          local: {
            userData: bh.local.userData,
          },
        }
      );
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_xdSgOzJUimaL6xuF');
    }
  }

  //appendnew_flow_restService_start

  async sd_tmq3a6l3nSLg77Z3(bh) {
    try {
      let basePath = bh.system.environment.properties.ssdURL.endsWith('/')
        ? bh.system.environment.properties.ssdURL
        : bh.system.environment.properties.ssdURL + '/';
      let url = `addincident/`;
      let finalUrl = basePath + url;
      let requestOptions = {
        url: finalUrl,
        method: 'post',
        responseType: 'text',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.incident,
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      bh = await this.sd_kgplAGb1ZS0t6NS1(bh);
      //appendnew_next_sd_tmq3a6l3nSLg77Z3
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_tmq3a6l3nSLg77Z3');
    }
  }

  async sd_kgplAGb1ZS0t6NS1(bh) {
    try {
      this.matSnackBar.open('Incident added', 'close to', {
        duration: 720,
        direction: 'ltr',
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
      //appendnew_next_sd_kgplAGb1ZS0t6NS1
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_kgplAGb1ZS0t6NS1');
    }
  }

  async sd_0i1l8s1OsOAlqCzs(bh) {
    try {
      let basePath = bh.system.environment.properties.ssdURL.endsWith('/')
        ? bh.system.environment.properties.ssdURL
        : bh.system.environment.properties.ssdURL + '/';
      let url = `getallincident/`;
      let finalUrl = basePath + url;
      let requestOptions = {
        url: finalUrl,
        method: 'get',
        responseType: 'json',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: undefined,
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_0i1l8s1OsOAlqCzs
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_0i1l8s1OsOAlqCzs');
    }
  }

  async sd_sVpgJ4PjgZeH4opj(bh) {
    try {
      let basePath = bh.system.environment.properties.ssdURL.endsWith('/')
        ? bh.system.environment.properties.ssdURL
        : bh.system.environment.properties.ssdURL + '/';
      let url = `updateincident/`;
      let finalUrl = basePath + url;
      let requestOptions = {
        url: finalUrl,
        method: 'put',
        responseType: 'text',
        reportProgress: undefined,
        headers: {},
        params: {},
        body: bh.input.incident,
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      bh = await this.dataUpdate(bh);
      //appendnew_next_sd_sVpgJ4PjgZeH4opj
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_sVpgJ4PjgZeH4opj');
    }
  }

  async dataUpdate(bh) {
    try {
      this.matSnackBar.open('Incident is updated', 'close', {
        duration: 720,
        direction: 'ltr',
        horizontalPosition: 'center',
        verticalPosition: 'bottom',
      });
      //appendnew_next_dataUpdate
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_zBsX980qKHvGJ1sx');
    }
  }

  async sd_8XZdP0HZxooR0xPL(bh) {
    try {
      console.log(bh.input.id);
      bh.local.qparams = {
        id: bh.input.id,
      };
      bh = await this.sd_BoiU7XQBaJdie4qD(bh);
      //appendnew_next_sd_8XZdP0HZxooR0xPL
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_8XZdP0HZxooR0xPL');
    }
  }

  async sd_BoiU7XQBaJdie4qD(bh) {
    try {
      let basePath = bh.system.environment.properties.ssdURL.endsWith('/')
        ? bh.system.environment.properties.ssdURL
        : bh.system.environment.properties.ssdURL + '/';
      let url = `deleteincident/`;
      let finalUrl = basePath + url;
      let requestOptions = {
        url: finalUrl,
        method: 'delete',
        responseType: 'text',
        reportProgress: undefined,
        headers: {},
        params: bh.local.qparams,
        body: undefined,
      };
      bh.local.result = await this.sdService.nHttpRequest(requestOptions);
      //appendnew_next_sd_BoiU7XQBaJdie4qD
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_BoiU7XQBaJdie4qD');
    }
  }

  async sd_Wil4WWsnczU8Pokg(bh) {
    try {
      localStorage.setItem('isLogin', JSON.stringify(bh.input.isLogin));
      //appendnew_next_sd_Wil4WWsnczU8Pokg
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_Wil4WWsnczU8Pokg');
    }
  }

  async sd_oNDJgAFs21TP0wSA(bh) {
    try {
      bh.local.isLogin = JSON.parse(localStorage.getItem('isLogin'));
      //appendnew_next_sd_oNDJgAFs21TP0wSA
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_oNDJgAFs21TP0wSA');
    }
  }

  async sd_BzdqWFx0Rv9j8eH6(bh) {
    try {
      localStorage.setItem('userData', JSON.stringify(bh.input.userData));
      //appendnew_next_sd_BzdqWFx0Rv9j8eH6
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_BzdqWFx0Rv9j8eH6');
    }
  }

  async sd_K3eU6nABk8arZwlC(bh) {
    try {
      bh.local.userData = JSON.parse(localStorage.getItem('userData'));
      //appendnew_next_sd_K3eU6nABk8arZwlC
      return bh;
    } catch (e) {
      return await this.errorHandler(bh, e, 'sd_K3eU6nABk8arZwlC');
    }
  }

  //appendnew_node

  async errorHandler(bh, e, src) {
    console.error(e);
    bh.error = e;
    bh.errorSource = src;

    if (
      false
      /*appendnew_next_Catch*/
    ) {
      return bh;
    } else {
      throw e;
    }
  }
  //appendnew_flow_restService_Catch
}
