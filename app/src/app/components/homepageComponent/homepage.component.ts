/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { NeutrinosOAuthClientService } from 'neutrinos-oauth-client';
import { Router } from '@angular/router';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-homepage',
    templateUrl: './homepage.template.html'
})

export class homepageComponent extends NBaseComponent implements OnInit {

    userInfo:any = {};

    constructor(private auth: NeutrinosOAuthClientService,private router:Router) {
        super();
    }

    ngOnInit() {
        this.userInfo = this.auth.userInfo;
        if(this.userInfo.teams[4].displayName.match('ima-admins')){
            this.router.navigate(['/home/list']);
        }
        else{
            this.router.navigate(['/home']);
        }
    }
}
