/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { incidentformpageComponent } from '../incidentformpageComponent/incidentformpage.component';
import { NeutrinosOAuthClientService } from 'neutrinos-oauth-client';
import { restService } from 'app/sd-services/restService';
import { Router } from '@angular/router';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-navbarpage',
    templateUrl: './navbarpage.template.html'
})

export class navbarpageComponent extends NBaseComponent implements OnInit {

    userInfo:any = {};
    isLogin:boolean=false;
    constructor(public dialog: MatDialog,private auth: NeutrinosOAuthClientService,private restService:restService,private router:Router) {
        super();
    }

    ngOnInit() {
        this.userInfo = this.auth.userInfo;
        console.log(this.userInfo);
        if(this.userInfo){
            this.isLogin = true;
        }


    }
    /* open dialog */
    openDialog() {
       if(this.auth.userInfo){
            const dialogRef = this.dialog.open(incidentformpageComponent,{
                data: {
                    id: null,
                }
            });
       }
       else{
           alert('Please login')
       }
    }
    async login(){
        await this.auth.login('');
        // this.restService.setData(this.auth.userInfo);
        this.restService.setLogin(true);
        this.isLogin = true;
    }
    async logout(){
        await this.auth.logout('home');
        this.isLogin = false;
    }
    goToList(){
        if(this.userInfo.teams[4].displayName.match('ima-admins')){
            this.router.navigate(['/home/list']);
        }
        else{
           alert('User not Authorised')
       }
    }
}
