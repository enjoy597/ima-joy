/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit,AfterViewInit,ViewChild } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import { restService } from '../../sd-services/restService';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { incidentformpageComponent } from '../incidentformpageComponent/incidentformpage.component';
import { confrimdeletepageComponent } from '../confrimdeletepageComponent/confrimdeletepage.component';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-incidentlistpage',
    templateUrl: './incidentlistpage.template.html'
})

export class incidentlistpageComponent extends NBaseComponent implements OnInit,AfterViewInit {

        dataList: any = [
        // {
        //     subject: 'Incident 1',
        //     priority: 1,
        //     description: 'This is incident one',
        //     date:'21st Jan 2021'
        // },
        // {
        //     subject: 'Incident 2',
        //     priority: 2,
        //     description: 'This is incident two',
        //     date:'2nd Feb 2021'
        // },
        // {
        //     subject: 'Incident 3',
        //     priority: 3,
        //     description: 'This is incident three',
        //     date:'4th March 2021'
        // },
        //         {
        //     subject: 'Incident 4',
        //     priority: 4,
        //     description: 'This is incident four',
        //     date:'1st March 2021'
        // },
        // {
        //     subject: 'Incident 5',
        //     priority: 5,
        //     description: 'This is incident five',
        //     date:'25nd Feb 2021'
        // },
        // {
        //     subject: 'Incident 6',
        //     priority: 6,
        //     description: 'This is incident five',
        //     date:'6th March 2021'
        // },
        // {
        //     subject: 'Incident 7',
        //     priority: 7,
        //     description: 'This is incident seven',
        //     date:'1st March 2021'
        // },
        // {
        //     subject: 'Incident 8',
        //     priority: 8,
        //     description: 'This is incident eight',
        //     date:'25nd Feb 2021'
        // },
        // {
        //     subject: 'Incident 9',
        //     priority: 9,
        //     description: 'This is incident nine',
        //     date:'6th March 2021'
        // }
    ];
    dataSource = new MatTableDataSource(this.dataList);
    @ViewChild(MatSort,{static: true}) sort: MatSort;
    @ViewChild(MatPaginator,{static: true}) paginator: MatPaginator;

    constructor(private restService:restService,private dialog: MatDialog) {
        super();
    }

    ngOnInit() {
        this.getData();
    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }
    /* filter data */
    applyFilter(event: Event) {
        console.log(event)
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    /* save data into database */
    async getData(){
        let data = await this.restService.getAllIncident();
        this.dataList = data.local.result;
        this.dataSource = new MatTableDataSource(this.dataList)
        console.log(this.dataList);
    }
    /* open dialgog to update */
    openDialog(incident) {
        console.log(incident)
         const dialogRef = this.dialog.open(incidentformpageComponent, {
            data: {
                id: incident.id,
                subject:incident.subject,
                description:incident.description,
                priority: incident.priority,
                date: incident.date_of_incident
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.getData();
        });
    }
    /* open dialog to confirm delete */
    conformDelete(incident){
        console.log(incident)
        const dialogRef = this.dialog.open(confrimdeletepageComponent, {
            data: {
                id: incident.id,
                subject:incident.subject
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.getData();
        });
    }
}
