/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component,Inject, OnInit } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { restService } from '../../sd-services/restService';
import * as moment from 'moment';
import { NeutrinosOAuthClientService } from 'neutrinos-oauth-client';
import { Router } from '@angular/router';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-incidentformpage',
    templateUrl: './incidentformpage.template.html'
})

export class incidentformpageComponent extends NBaseComponent implements OnInit {

    incidentform;
    toDaysDate = new Date();
    userData:any = {};
    constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<incidentformpageComponent>, private auth:NeutrinosOAuthClientService,
    private restService: restService,@Inject(MAT_DIALOG_DATA) public data,private router:Router) {
        super();
    }

    ngOnInit() {
        this.createForm();
        if(this.data.id != null){
            console.log(this.data)
            this.incidentform.patchValue(this.data)
        }
        this.userData = this.auth.getUserInfo();
    }

    /* create login form */
    createForm() {
        this.incidentform = this.fb.group({
            id: [null],
            subject: ['', [Validators.required, Validators.pattern(`.*[^0-9]`)]],
            description: ['', Validators.required],
            priority: ['', Validators.required],
            date: ['', Validators.required]
        })
    }
    /* validatte form fields */
    validateField() {
        Object.keys(this.incidentform.controls).forEach(field => {
            const controls = this.incidentform.get(field);
            controls.markAsTouched({ onlySelf: true });
        })
    }
    /* submit form */
    onSubmit() {
        console.log(this.incidentform.value);
        // console.log('Form is invalid or not -- ' + this.incidentform.invalid)''
        if (this.incidentform.invalid) {
            this.validateField();
        }
        else {
            if(this.incidentform.controls.id.value === null){
                this.saveData();
            }
            else{
                this.updateData();
            }
        }
    }
    /* save data into database */
    async saveData() {
        await this.restService.addIncident(this.incidentform.value);
        this.dialogRef.close();
        this.refresh();
    }
    /* reload page */
    refresh(): void {
        window.location.reload();
    }
    checkRole(){
        if(this.userData.teams[3].displayName.matchj('ima-admins')){
            this.router.navigate(['/home/list']);
        }
    }
    /* save data into database */
    async updateData() {
        await this.restService.updateIncident(this.incidentform.value);
        this.dialogRef.close();
    }
}
