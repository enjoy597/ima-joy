/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit,Inject } from '@angular/core'
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { restService } from '../../sd-services/restService';

/*
Client Service import Example:
import { servicename } from 'app/sd-services/servicename';
*/

/*
Legacy Service import Example :
import { HeroService } from '../../services/hero/hero.service';
*/

@Component({
    selector: 'bh-confrimdeletepage',
    templateUrl: './confrimdeletepage.template.html'
})

export class confrimdeletepageComponent extends NBaseComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data, public dialogRef: MatDialogRef<confrimdeletepageComponent>,private restService:restService) {
        super();
    }

    ngOnInit() {

    }
    /* close dialog */
    closeDialog(){
        this.dialogRef.close();
    }
    /* delete api call */
    async deleteIncident(){
        let data = await this.restService.deleteIncident(this.data.id);
        console.log(data);
        this.closeDialog();
    }
}
