import { NeutrinosAuthGuardService } from 'neutrinos-oauth-client';
import { PageNotFoundComponent } from '../not-found.component';
import { LayoutComponent } from '../layout/layout.component';
import { ImgSrcDirective } from '../directives/imgSrc.directive';
import { APP_INITIALIZER } from '@angular/core';
import { NDataSourceService } from '../n-services/n-dataSorce.service';
import { environment } from '../../environments/environment';
import { NLocaleResource } from '../n-services/n-localeResources.service';
import { NAuthGuardService } from 'neutrinos-seed-services';
import { ArtImgSrcDirective } from '../directives/artImgSrc.directive';


window['neutrinos'] = {
  environments: environment
}

//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-loginpageComponent
import { loginpageComponent } from '../components/loginpageComponent/loginpage.component';
//CORE_REFERENCE_IMPORT-confrimdeletepageComponent
import { confrimdeletepageComponent } from '../components/confrimdeletepageComponent/confrimdeletepage.component';
//CORE_REFERENCE_IMPORT-homepageComponent
import { homepageComponent } from '../components/homepageComponent/homepage.component';
//CORE_REFERENCE_IMPORT-incidentformpageComponent
import { incidentformpageComponent } from '../components/incidentformpageComponent/incidentformpage.component';
//CORE_REFERENCE_IMPORT-incidentlistpageComponent
import { incidentlistpageComponent } from '../components/incidentlistpageComponent/incidentlistpage.component';
//CORE_REFERENCE_IMPORT-navbarpageComponent
import { navbarpageComponent } from '../components/navbarpageComponent/navbarpage.component';

/**
 * Reads datasource object and injects the datasource object into window object
 * Injects the imported environment object into the window object
 *
 */
export function startupServiceFactory(startupService: NDataSourceService) {
  return () => startupService.getDataSource();
}

/**
*bootstrap for @NgModule
*/
export const appBootstrap: any = [
  LayoutComponent,
];


/**
*declarations for @NgModule
*/
export const appDeclarations = [
  ImgSrcDirective,
  LayoutComponent,
  PageNotFoundComponent,
  ArtImgSrcDirective,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-loginpageComponent
loginpageComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-confrimdeletepageComponent
confrimdeletepageComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-homepageComponent
homepageComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-incidentformpageComponent
incidentformpageComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-incidentlistpageComponent
incidentlistpageComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-navbarpageComponent
navbarpageComponent,

];

/**
* provider for @NgModuke
*/
export const appProviders = [
  NDataSourceService,
  NLocaleResource,
  {
    // Provider for APP_INITIALIZER
    provide: APP_INITIALIZER,
    useFactory: startupServiceFactory,
    deps: [NDataSourceService],
    multi: true
  },
  NAuthGuardService,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY

];

/**
* Routes available for bApp
*/

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_START
export const appRoutes = [{path: 'home', component: homepageComponent, canActivate: [NeutrinosAuthGuardService]},{path: 'home/list', component: incidentlistpageComponent, canActivate: [NeutrinosAuthGuardService]},{path: '', redirectTo: 'home', pathMatch: 'full'},{path: '**', component: PageNotFoundComponent}]
// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_END
